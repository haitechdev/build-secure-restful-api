
const config = {
    production: {
        secret: process.env.secret,
        MONGODB_URI: process.env.MONGODB_URI,
        port: process.env.PORT
    },
    development: {
        secret: 'I_AM_SECRET',
        MONGODB_URI: 'mongodb://mhaitana:Haitana12@ds211694.mlab.com:11694/heroku_bz7sq045',
        port: 3000
    }
};

export const getConfig = env => config[env] || config.development
