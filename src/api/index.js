import express from 'express';
import { songRouter } from './resources/song';
import { userRouter } from './resources/user';
import { playlistRouter } from './resources/playlist';
export const restRouter = express.Router();

restRouter.use('/songs', songRouter);
restRouter.use('/users', userRouter);
restRouter.use('/playlists', playlistRouter);
