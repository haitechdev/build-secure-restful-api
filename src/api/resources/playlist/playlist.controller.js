import Joi from 'joi';
import Playlist from './playlist.model';
import playlistService from './playlist.service';

export default {
    async create(req, res) {
        try {
            const { value, error } = playlistService.validatePlaylist(req.body);

            if (error && error.details) {
                return res.status(400).json(error);
            }

            const playlist = await Playlist.create(Object.assign({}, value, { user: req.user._id }));

            return res.json(playlist);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async findAll(req, res) {
        try {
            const playlists = await Playlist.find()
                .populate('songs')
                .populate('user', 'firstName lastName');

            return res.json(playlists);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async findOne(req, res) {
        try {
            const { id } = req.params;
            const playlist = await Playlist.findById(id)
                .populate('artist', 'firstName lastName');

            if (!playlist) {
                return res.status(404).json({ err: 'could not find playlist'});
            }

            return res.json(playlist);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async delete(req, res) {
        try {
            const { id } = req.params;
            const playlist = await Playlist.findByIdAndDelete(id);

            if (!playlist) {
                return res.status(404).json({ err: 'could not find playlist'});
            }

            return res.json(playlist);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async update(req, res) {
        try {
            const schema = Joi.object().keys({
                title: Joi.string().required(),
                url: Joi.string().required(),
                rating: Joi.number()
                    .integer()
                    .min(0)
                    .max(5)
                    .optional()
            })
            const { value, error } = Joi.validate(req.body, schema);

            if (error && error.details) {
                return res.status(400).json(error);
            }

            const { id } = req.params;
            const playlist = await Playlist.findOneAndUpdate({ _id: id }, value, { new: true });

            if (!playlist) {
                return res.status(404).json({ err: 'could not find playlist'});
            }

            return res.json(playlist);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    }
}
