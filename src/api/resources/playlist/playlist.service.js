import Joi from 'joi';

export default {
    validatePlaylist(body) {
        const schema = Joi.object().keys({
            name: Joi.string().required(),
            songs: Joi.array().items().required()
        })

        const { value, error } = Joi.validate(body, schema);
        if (error && error.details) {
            return { error };
        }

        return { value };
    }
}
