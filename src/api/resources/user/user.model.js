import mongoose from 'mongoose';

export const STANDARD_ROLE = 2;
export const ARTIST_ROLE = 1; 

const { Schema } = mongoose;
const userSchema = new Schema({
    firstName: {
        type: String,
        required: [true, 'User must have firstName']
    },
    lastName: {
        type: String,
        required: [true, 'User must have lastName']
    },
    email: {
        type: String,
        required: [true, 'User must have email'],
        unique: [true, 'Email must be unique']
    },
    password: {
        type: String,
        required: [true, 'User must have password']
    },
    role: {
        default: 2,
        required: [true, 'User must have role'],
        type: Number,
    }
});

export default mongoose.model('User', userSchema);
