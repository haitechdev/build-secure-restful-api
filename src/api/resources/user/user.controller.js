import User, { STANDARD_ROLE } from './user.model';
import userService from './user.service';
import jwt from '../../helpers/jwt';

export default {
    async signup(req, res) {
        try {
            const { value, error } = userService.validateSignup(req.body);

            if (error) {
                return res.status(400).send(error);
            }

            const encryptedPassword = userService.encryptPassword(value.password);
            const user = await User.create({
                email: value.email,
                firstName: value.firstName,
                lastName: value.lastName,
                password: encryptedPassword,
                role: value.role || STANDARD_ROLE
            });
            return res.json({ success: true });
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async login(req, res) {
        try {
            const { value, error } = userService.validateLogin(req.body);

            if (error) {
                return res.status(400).send(error);
            }

            const user = await User.findOne({ email: value.email });

            if (!user) {
                return res.status(401).json({ error: 'unauthorized' });
            }

            const authenticated = userService.comparePassword(value.password, user.password);

            if (!authenticated) {
                return res.status(401).json({ error: 'unauthorized' });
            }

            const token = jwt.issue({ id: user._id }, '1d');

            return res.json(token);
        }
        catch (err) {
            console.error(err);
            return res.status(500).send(err);
        }
    },
    async authenticate(req, res) {
        return res.json(req.user);
    }
}
